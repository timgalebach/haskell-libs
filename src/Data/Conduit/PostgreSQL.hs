module Data.Conduit.PostgreSQL
       (
         sourceQuery_
       , sourceQuery
       , sinkUpdate
       , UpdateTuple
       ) where
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

import Database.PostgreSQL.Simple (Connection, Query, connectPostgreSQL, close
                                  , forEach, forEach_, executeMany)
import Database.PostgreSQL.Simple.FromRow
import Database.PostgreSQL.Simple.ToRow
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad (join)
import Data.ByteString (ByteString(..))
import qualified Data.Vector as V
import Data.Conduit.TMChan (sourceTMChan)
import Control.Concurrent.STM.TMChan (newTMChanIO, writeTMChan, closeTMChan, TMChan)
import GHC.Conc (atomically, forkIO)
import Data.Conduit (bracketP)
import Data.Void (Void(..))
import Conduit

class UpdateTuple a where
  toTuple :: (ToRow t) => a -> t

--closes the channel after action is done to terminate the source automatically
pgChan :: (MonadResource m,  FromRow r) =>
              ((r -> IO ()) -> IO ()) -> IO (Source m r)
pgChan queryFn = do
  chan <- newTMChanIO
  _ <- forkIO $ do queryFn $ atomically . (writeTMChan chan)
                   atomically $ closeTMChan chan
  pure $ sourceTMChan chan

query :: (ToRow params, FromRow r, MonadResource m) =>
         params -> Connection -> Query -> IO (Source m r)
query params conn q = pgChan $ forEach conn q params

query_ :: (FromRow r, MonadResource m) => Connection -> Query -> IO (Source m r)
query_ conn q = pgChan $ forEach_ conn q

mkSource :: (FromRow row, MonadResource m) =>
            (Connection -> Query -> IO (Source m row)) ->
            ByteString  -> Query -> ConduitM () row m ()
mkSource queryFn connStr q = do
  bracketP (connectPostgreSQL connStr) close (go q)
    where go q conn = join $ liftIO $ queryFn conn q

sourceQuery_ :: (MonadResource m, FromRow row) =>
                ByteString -> Query -> ConduitM () row m ()
sourceQuery_ = mkSource query_

sourceQuery :: (MonadResource m, FromRow row, ToRow params) =>
                ByteString -> Query -> params -> ConduitM () row m ()
sourceQuery connStr q params = mkSource (query params) connStr q

sinkUpdate :: (MonadResource m, ToRow t) =>
                   ByteString -> Query -> ConduitM (V.Vector t) Void m ()
sinkUpdate connStr updateQ = do
  bracketP (connectPostgreSQL connStr) close (go updateQ)
    where go q conn = do
            maybeUpdateVals <- await
            case maybeUpdateVals of
              Nothing  -> pure ()
              Just vec -> do
                liftIO $ executeMany conn q $ V.toList vec
                pure ()
